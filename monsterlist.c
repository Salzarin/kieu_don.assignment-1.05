#include "monster.h"

int queue_init(monster_list_t *e){
  e->size = 0;
  e->head = e->tail = NULL;
  return 0;
}
int queue_delete(monster_list_t *e){
  monster m;
  while(!queue_remove(e,&m))
   ;
  return 0;
}

int queue_add(monster_list_t *e,monster_node_t ** mn, monster m){
   if(e->head){
      monster_node_t* temp = malloc(sizeof (monster_node_t));
      e->tail->next = temp;
      temp->prev = e->tail;
      temp->next = NULL;
      e->tail = temp;
      e->tail->data = m;
      *mn=e->tail;
      e->size++;    
  }
  else{
    e->head = malloc(sizeof (monster_node_t));
    e->head->next = NULL;
    e->head->prev = NULL;
    e->head->data = m;
    *mn=e->head;
    e->tail = e->head;
    e->size++;
  }
  
  
  return 0;
}


monster queue_at(monster_node_t * at){
  monster m;
  if(!at){
    return at->data;
  }
  return m;
}

int queue_remove(monster_list_t *e, monster *m){
    monster_node_t *n;
    
    if(!e->size){
      return 1;
    }
    n=e->head;
    e->head = e->head->next;
    //e->head->prev = NULL;
    e->size--;
    *m = n->data;
    free(n);
    if(!e->size){
      e->tail = NULL;
    }
  return 0;
}

int queue_remove_at(monster_list_t *e, monster_node_t *m){
    
    if(!e->size){
      return 1;
    }
    if(m==e->head){
      monster n;
      queue_remove(e,&n);
    }
    else if(m==e->tail){
    m->prev->next = NULL;
    e->tail = m->prev;
    e->size--;
    free(m);
    m = NULL;
      
    }
    else{
    m->prev->next = m->next;
    m->next->prev = m->prev;
    e->size--;
    free(m);
    m = NULL;
    }
    return e->size;
}

int queue_size(monster_list_t *e){ 
  return e->size;
}


