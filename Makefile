all:	Dungeon
	


Dungeon: main.c dungeon.c dijkstras.c adjacencylist.c monster.c player.c characterlist.h characterlist.c monsterlist.c
	gcc -g -Wall -lncurses -o Dungeon main.c dungeon.c monster.c dijkstras.c adjacencylist.c player.c characterlist.c monsterlist.c
clean:
	rm -f Dungeon *o *~
tar:
	tar cvfz ./../kieu_don.assignment-1.05.tar.gz ./../kieu_don.assignment-1.05/ --exclude .git
changelog:
	git log --date=local --pretty=format:"%ad, %h, %an, message: %s" > CHANGELOG
