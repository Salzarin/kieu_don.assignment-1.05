#include "dungeon.h"


int main(int argc, char * argv[]){
  
  //Initialize Time for Rand
  
  struct timespec ti;
  //time_t ti;
  clock_gettime(CLOCK_REALTIME, &ti);

  //Seed the RNG
  srand((unsigned) time(&ti.tv_sec));
  
  int i;
  bool save = false;
  bool load = false;
  bool player_set = false;
  bool verbose_flag = false;
  bool player_auto = false;
  
  
  dungeon_t d;
  d.Max_Rooms = 5;

  monster_list_t* mList;
  monster_node_t ** monsters;

  
  char * filename = malloc(50); //Guarantee Memory Space
  strcpy(filename,"dungeon");
  player p;
  
  int nummon = 5;
  int p_speed = 10;
  
  
  for(i = 1;i<argc;i++){
    if(!strcmp(argv[i],"--save")){
      save = true;
    }
    else if(!strcmp(argv[i],"--load")){
      load = true;
    }
    else if(!strcmp(argv[i],"-s")){
      if(i+1<argc){
	d.Max_Rooms = atoi(argv[i+1]) > 5 ? atoi(argv[i+1]) : 5;
      } 
    }    
    else if(!strcmp(argv[i], "-l")){
      if(i+1<argc){
	//filename = argv[i+1];
	if(filename[0] != '-' && strcmp(argv[i+1],"")){ 
	  //filename = argv[i+1];
	  memset(filename,'\0',50);
	  strcpy(filename,argv[i+1]);
	}
	else{
	  printf("Invalid Load File, using dungeon\n");
	  memset(filename,'\0',50);
	  strcpy(filename,"dungeon");
	}
      }   
    }
    else if(!strcmp(argv[i], "-p")){
      if(i+2<argc){
	p.x = atoi(argv[i+1]);
	p.y = atoi(argv[i+2]);
	player_set = true;
      }   
    }
    else if(!strcmp(argv[i], "-nummon")){
      if(i+1<argc){
	nummon = atoi(argv[i+1]);
      }
    }
    else if(!strcmp(argv[i], "-pspeed")){
      if(i+1<argc){
	p_speed = atoi(argv[i+1]);
      }
    }
    else if(!strcmp(argv[i], "-v")){
      verbose_flag = true;
    }
    else if(!strcmp(argv[i], "-auto")){
      player_auto = true;
    }
  }
  
  
  //monster monsters[5000];
  
  
  //Initialize Dungeon Map
  memset(d.dungeonmap,0,sizeof(d.dungeonmap));
  nummon=nummon>0?nummon:1;
  p_speed=p_speed>0?p_speed:10;
  
   
  
if(!load){
  printf("Creating Dungeon\n");
  d.Max_Rooms = createDungeon(&d);
}
else{

  printf("Loading Dungeon\n");
  int testMax_Rooms = loadDungeon(filename, &d);
  if(testMax_Rooms == -1){
    testMax_Rooms = d.Max_Rooms;
    printf("Creating Dungeon\n");
    d.Max_Rooms = createDungeon(&d);
  }
  else{
  
    d.Max_Rooms = testMax_Rooms;
  }  
}

if(save){
  printf("Saving Dungeon\n");  
  saveDungeon(filename, &d);    
}
  
  free(filename);

  setPlayerPosition(&p,d, player_set, 0);


  
  
  
  
  initscr();
  raw();
  start_color();
  initAllColors();

  
  //timeout(0);
  

  character events[5000];
  int eventSize = 0;
  int eventPos[5000];
  
  p._isAlive = true;
  p.speed = p_speed;
  
  mList = malloc(sizeof(monster_list_t));
  monsters = malloc(sizeof(monster_node_t)*(nummon));
  
  init_Events(p,d,events, &eventSize,eventPos, mList, monsters, &nummon, ti);

  
  
  
  noecho();
  bool gameover = false;
  curs_set(0);
  int dest_room = 0;
  

  player p_dest;
  p_dest.x = d.Room[dest_room].x;
  p_dest.y = d.Room[dest_room].y;
  int dummyTunnel[MAX_H][MAX_V];
  createNoTunnelMap(p_dest, d.rockhardnessmap,dummyTunnel,d.PDestMap);
  
  
  int key = 0;
  
  while(key!='Q' && !gameover){
    
    renderDungeon(p,d,mList);
    
    //Event Queue
    cBuildHeap(events,eventSize,eventPos);
    character t = cPop(events,&eventSize,eventPos);
    
    for(i = 0;i<eventSize;i++){
	events[i].eventTime = (events[i].eventTime - t.eventTime) < 0 ? 0 :(events[i].eventTime - t.eventTime)  ;  
    }
    
    
     //Can Monsters see Player
    bool gcheck = queue_size(mList) > 0 ? false :true;
    

    gameover = gcheck;
    
    

    int new_x = 0;
    int new_y = 0;
    int pdx = 0;
    int pdy = 0; 
    
    
   if(t.player == true){
    
    flushinp();
    key = 0;
    bool add_event = true;
    
    if(!player_auto){
      while(!validKeyCheck(key) || (new_x<1 || new_x>(MAX_H-2) || new_y< 1 || new_y >(MAX_V-2))){
	move(22+2,0);
	printw("Please Select Action for Player. Press 5 to wait or press q to quit.\n");
	printw("7  8  9  y  k  u\n %c | /    %c | / \n4--5--6  h-- --l\n / | %c    / | %c \n1  2  3  b  j  n\n", 92 , 92,92,92);
	
	if(gcheck){
	  key='5'; //Wait this turn to process Game Over.
	}
	else{
	  key = getch();
	}
	
	if(key == 'm'){
	  key = printMonsterList(p, mList);
	  renderDungeon(p,d,mList);
	}
	
	if(key == '<'){
	  if(d.dungeonmap[p.x][p.y] == '<'){
	    
	    d.Max_Rooms = createDungeon(&d);
	    
	    setPlayerPosition(&p,d, player_set, 1);
	    
	    cleanEvents(events, &eventSize, eventPos, mList,monsters);
	    mList = malloc(sizeof(monster_list_t));
	    monsters = malloc(sizeof(monster_node_t)*(nummon));
	    init_Events(p,d,events, &eventSize,eventPos, mList, monsters, &nummon, ti);
	    key = ' ';
	    add_event = false;
	  }
	}
	
	if(key == '>'){
	
	    if(d.dungeonmap[p.x][p.y] == '>'){
	      d.Max_Rooms = createDungeon(&d);
	      setPlayerPosition(&p,d, player_set, -1);
	      cleanEvents(events, &eventSize, eventPos, mList,monsters);
	      mList = malloc(sizeof(monster_list_t));
	      monsters = malloc(sizeof(monster_node_t)*(nummon));
	      init_Events(p,d,events, &eventSize,eventPos, mList, monsters, &nummon, ti);
	      key = ' ';
	      add_event = false;
	  }
	}
	
	
	
	pdx = 0;
	pdy = 0;
	getnewDirection(translateKeyinput(key),&pdx,&pdy);
	
	new_x = p.x+pdx;
	new_y = p.y+pdy;
	
	if(d.rockhardnessmap[new_x][new_y] !=0){
	  key = 0;
	}
	
      }
    }
    else{
      while((new_x<1 || new_x>(MAX_H-2) || new_y< 1 || new_y >(MAX_V-2))){
	
	if(d.Room[dest_room].x  == p.x && d.Room[dest_room].y  == p.y){
	  dest_room = (dest_room +1) % d.Max_Rooms;
	  player p_dest;
	  p_dest.x = d.Room[dest_room].x;
	  p_dest.y = d.Room[dest_room].y;
	  createNoTunnelMap(p_dest, d.rockhardnessmap,dummyTunnel,d.PDestMap);
	}
	
	if(0){
	key = rand() % 8 + '0';
	pdx = 0;
	pdy = 0;
	getnewDirection(translateKeyinput(key),&pdx,&pdy);
	
	new_x = p.x+pdx;
	new_y = p.y+pdy;
	}
	else{
	  move(27,0);
	  
	  printw("%d : %d", d.PDestMap[p.x+p.y*MAX_H]%MAX_H, d.PDestMap[p.x+p.y*MAX_H]/MAX_H);
	  new_x = d.PDestMap[p.x+p.y*MAX_H]%MAX_H;
	  new_y = d.PDestMap[p.x+p.y*MAX_H]/MAX_H;
	  pdx = new_x - p.x;
	  pdy = new_y - p.y;
	  
	  
	}
	
      }
    }
    

    
    if(key == '5' || key == ' '){
    }
    else{
      //Translate Key  
      if(!player_auto){
      pdx = 0;
      pdy = 0;
      getnewDirection(translateKeyinput(key),&pdx,&pdy);
      
	new_x = p.x+pdx;
	new_y = p.y+pdy;
      }
      if(d.rockhardnessmap[p.x+pdx][p.y+pdy] == 0){
	p.x=new_x;
	p.y=new_y;
	createTunnelMap(p, d.rockhardnessmap,d.TunnelMap,d.PTMap);
	createNoTunnelMap(p, d.rockhardnessmap,d.NoneTunnelMap,d.PNTMap);
	    if(verbose_flag){
	      move(21+1,0);
	      printw("Moved Player to x:%d y:%d with speed of %d.\n",p.x,p.y, t.eventTime);
      
	    }
      }
      else{
	/*
	    d.rockhardnessmap[new_x][new_y]-=85;
	    if(d.rockhardnessmap[new_x][new_y] <= 0){
	      d.rockhardnessmap[new_x][new_y] = d.rockhardnessmap[new_x][new_y] < 0 ? 0:d.rockhardnessmap[new_x][new_y];
	      d.dungeonmap[new_x][new_y] = '#';
	      createTunnelMap(p, d.rockhardnessmap,d.TunnelMap,d.PTMap);
	      createNoTunnelMap(p, d.rockhardnessmap,d.NoneTunnelMap,d.PNTMap);  
	      p.x = new_x;
	      p.y = new_y;
	      if(verbose_flag){
		move(21+1,0);
		printw("Tunneled and Moved Player to x:%d y:%d.\n",p.x,p.y);
      
	      }
	    }
	    else{
	      if(verbose_flag){
		move(21+1,0);
		printw("Player Attempted to Tunnel to x:%d y:%d.\n",p.x,p.y);
	      }
	      
	    }
	 */
      }
      
      
      monster_node_t* m_itr = mList->head;
      while(m_itr){
	if(p.x == m_itr->data.x && p.y == m_itr->data.y){
	  monster_node_t * temp = m_itr;
	  temp->data._isAlive = false;
	  for(i = 0;i<eventSize;i++){
	    if(events[i].mNode == temp){
	      events[i]._isAlive = false;
	    }
	  }
	  m_itr = m_itr->next;
	  queue_remove_at(mList, temp);
	}
	else{
	m_itr=m_itr->next;
	}
      }
    }
    
   if(add_event){
    t.eventTime = 1000/p.speed;
    clock_gettime(CLOCK_REALTIME,&ti);
    t.timeCreated = time(&ti.tv_sec);
    t.timeCreatedNano = ti.tv_nsec;
    cPush(events,t,&eventSize,eventPos);
   }
  }
   else{
     if(t._isAlive){
      monster cur_monster = t.mNode->data;
      
      if(cur_monster._isAlive){
	//Handle Monster Behaviour 
	new_x = cur_monster.x;
	new_y = cur_monster.y;
	
	t.mNode->data._seen = checkLos(&cur_monster,p,d);
	
	if(!(cur_monster.stats.attr & 0x01) && !cur_monster._seen){
	  cur_monster.dest_x = new_x;
	  cur_monster.dest_y = new_y; 
	}
	
	monsterMovement(p,&cur_monster,d.PNTMap,d.PTMap,d.rockhardnessmap,&new_x,&new_y);
	
	if(d.rockhardnessmap[new_x][new_y] == 0){
	    bool check = false;
	    //int i;
	    monster_node_t* m_itr = mList->head;
	    while(m_itr){
	      if(m_itr->data._isAlive && new_x==m_itr->data.x && new_y==m_itr->data.y ){
		check=true;
	      }
	      m_itr = m_itr->next;
	    }
	  
	  
	  if(cur_monster.x == cur_monster.dest_x &&
	    cur_monster.y == cur_monster.dest_y){
	    cur_monster._seen = false;  
	  }
	  
	  //Move Monster
	  if(!check){
	    cur_monster.x = new_x;
	    cur_monster.y = new_y;
	    if(verbose_flag){     
	      move(21+1,0);
	      printw("%1x Monster moved to x: %d y: %d with speed of %d\n",
		    cur_monster.stats.attr,
		    cur_monster.x,
		    cur_monster.y,
		    t.eventTime
		    );
	    }
	    
	    
	  }
	  else{
	    if(verbose_flag){     
	      move(21+1,0);
	      printw("%1x Monster stayed at x: %d y: %d with speed of %d\n",
		    cur_monster.stats.attr,
		    cur_monster.x,
		    cur_monster.y,
		    t.eventTime
		    );
	      
	    }
	    
	  }
	  
	  //Game Over Condition Flag Check
	  
	  if(new_x == p.x && new_y==p.y){
	    gameover = true;
	    p._isAlive = false;
	  }
	  
	}
	else{
	  if((cur_monster.stats.attr>>2) & 0x01){
	      d.rockhardnessmap[new_x][new_y]-=85;
	      if(d.rockhardnessmap[new_x][new_y] <= 0){
		d.rockhardnessmap[new_x][new_y] = d.rockhardnessmap[new_x][new_y] < 0 ? 0:d.rockhardnessmap[new_x][new_y];
		d.dungeonmap[new_x][new_y] = '#';
		createTunnelMap(p, d.rockhardnessmap,d.TunnelMap,d.PTMap);
		createNoTunnelMap(p, d.rockhardnessmap,d.NoneTunnelMap,d.PNTMap);
		cur_monster.x = new_x;
		cur_monster.y = new_y;
		
		if(verbose_flag){     
		  move(21+1,0);
		  printw("%1x Monster Tunneled and Moved to x: %d y: %d with speed of %d\n",
			cur_monster.stats.attr,
			cur_monster.x,
			cur_monster.y,
			cur_monster.speed
			);
		  
		}
		
	      }
	      else{
		if(verbose_flag){     
		  move(21+1,0);
		  printw("%1x Monster Attempting to Tunnel to x: %d y: %d with speed of %d\n",
			cur_monster.stats.attr,
			new_x,
			new_y,
			cur_monster.speed
			);  
		}
		
	      }
	    }
	    else{
	    if(verbose_flag){     
	      move(21+1,0);
	      printw("%1x Monster stayed at x: %d y: %d with speed of %d\n",
		    cur_monster.stats.attr,
		    cur_monster.x,
		    cur_monster.y,
		    cur_monster.speed
		    );
	      
	    }
	      
	    }
	    
	}
	
      cur_monster.inRoom = d.dungeonmap[cur_monster.x][cur_monster.y] == '.' ? true : false;
      
      t.mNode->data = cur_monster;
      t.eventTime = 1000/cur_monster.speed;
      clock_gettime(CLOCK_REALTIME,&ti);    
      t.timeCreated = time(&ti.tv_sec);
      t.timeCreatedNano = ti.tv_nsec;
      cPush(events,t,&eventSize,eventPos);     
	  
      }
      else{
      }
     }

   }   
    
    move(21+1,0);// Move to End of the line
    
    if(t.player ==true){
      //key = getch();     
    }
    
    refresh();
    if(!verbose_flag){
      if(t.eventTime> 0){ 
	usleep(t.eventTime*100);
      }
    }
    else{
      usleep(1000000);
    }
    
   
    if(player_auto){
    timeout(0);
      key = getch();
    }
  }
  
  gameoverMessage(p,d,mList);
  
  endwin();
  
  queue_delete(mList);
  
  free(mList);
  free(monsters);
  
return 0;
}