#include "dungeon.h"


int movePlayer(player *p, int dx, int dy,int hardmap[MAX_H][MAX_V],int dungeonmap[MAX_H][MAX_V]){
  
  if(hardmap[p->x+dx][p->y+dy] != 0){
    return 1;
  }
  p->x += dx;
  p->y += dy;
  return 0;
  
  
  
}


void printPlayer(player p){
    attron(A_BLINK);
    mvprintw(p.y+1,p.x,"@");
    attroff(A_BLINK); 
}


void setPlayerPosition(player *p, dungeon_t d, bool player_set, int Ascending){

  int cRoom = rand() % d.Max_Rooms;
  int x = d.Room[cRoom].x;
  int y = d.Room[cRoom].y;
  int w = d.Room[cRoom].w;
  int h = d.Room[cRoom].h;
  bool inRoom = false;
  
  if(!player_set){
    p->x = x + rand() % w;
    p->y = y + rand() % h;
  }
  else{
    if(d.rockhardnessmap[p->x][p->y] == 0){
      printf("located in room\n");
      inRoom = true;
    }
  }
  
  
  if(!inRoom){
    if(player_set){
    printf("-p %d %d arg is not in a room.\n", p->x, p->y);
    player_set = false;
    }
  }
  
  if(!player_set){
    p->x = x + rand() % w;
    p->y = y + rand() % h;
  }
  
  if(Ascending == -1){
  p->x = d.downStairs.x;
  p->y = d.downStairs.y;
  }
  else if(Ascending == 1){
  
  p->x = d.upStairs.x;
  p->y = d.upStairs.y;
    
  }
  else{
  }
  
  
}

