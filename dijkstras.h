#ifndef DIJKSTRAS_H
#define DIJKSTRAS_H

#define MAX_H 80
#define MAX_V 21
#define MAX_Q 5000

#include "adjacencylist.h"


typedef struct pList{
int List[MAX_H*MAX_V];
int size;
}pList;




pList dikstras(int map[MAX_H][MAX_V], int start, int end, bool monster, bool tunneler, int *length, int pathArray[MAX_H*MAX_V],int PMap[MAX_H*MAX_V] );



#endif
