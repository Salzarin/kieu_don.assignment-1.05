#include "characterlist.h"





int cleft(int i){

  return (2*i+1);
}

int cright(int i){

  return (2*i+2);
}

int cparent(int i){
  return (i-1)/2;
}


//organize a heap containing our cumulative distance
//traveled down nodes
void cHeap(character Heap[], int size, int check, int pos[]){
	
	//Temp Varible for swapping.
	character temp;
	
	int l = cleft(check);
	int r = cright(check);
	int index = check;
	
	if(l<size && Heap[l].eventTime < Heap[check].eventTime){
	    index = l;
	}
	else if(l<size &&  Heap[l].eventTime == Heap[check].eventTime){
	  if(l<size && Heap[l].timeCreated < Heap[check].timeCreated){
	    index = l;
	  }
	  else if(l<size && Heap[l].timeCreated == Heap[check].timeCreated){
	    if(l<size && Heap[l].timeCreatedNano < Heap[check].timeCreatedNano){
	      index = l;
	    }
	  }
	  
	}
	
	if(r<size && Heap[r].eventTime <= Heap[index].eventTime){

	  index = r;
	}
	else if(r<size && Heap[r].eventTime == Heap[check].eventTime){
	  if(r<size &&Heap[r].timeCreated < Heap[check].timeCreated){
	    index = r;
	  }
	  else if(r<size &&Heap[r].timeCreated == Heap[check].timeCreated){
	    if(r<size &&Heap[r].timeCreatedNano < Heap[check].timeCreatedNano){
	      index = r;
	    }
	  }
	  
	}
	
	if(index!=check){
	  
	  
	  pos[Heap[check].id] = index;
	  pos[Heap[index].id] = check;
	  
	  temp = Heap[check];
	  Heap[check]=Heap[index];
	  Heap[index] = temp;
	  cHeap(Heap,size,index,pos);
	  
	}

}


//decrease key command. Rebuilds the heap and pushes the
//smallest value to the top of the stack.
void cDecreaseKey(character Heap[], character Node, int pos[]){
	
	int i = pos[Node.id];
	character temp;

	Heap[i].eventTime = Node.eventTime;
	
	while(i!=0 && Heap[cparent(i)].eventTime > Heap[i].eventTime){
	      pos[Heap[i].id] = cparent(i);
	      pos[Heap[cparent(i)].id] = i;
	      
	      temp = Heap[cparent(i)];
	      Heap[cparent(i)] = Heap[i];
	      Heap[i] = temp;
	      i = cparent(i);
	}
}
//Function to get the smallest value
//With the heap, should be at the head of the heap.
character cPop(character Heap[], int *size, int pos[]){
	
	pos[Heap[0].id] = *size-1;
	pos[Heap[*size-1].id] = 0;
	
	character min=Heap[0];
	Heap[0] = Heap[*size-1];
	--(*size);
	cHeap(Heap,*size,0,pos);
	return min;
}


void cPush(character Heap[],character c, int * size, int pos[]){
	(*size)++;
	Heap[*size-1] = c;
	pos[c.id] = *size-1;
	cHeap(Heap,*size,*size-1,pos);
}


//Construct the heap.
void cBuildHeap(character Heap[], int size, int pos[]){
	int i;
	
	for(i = size;i>=0;--i){
		cHeap(Heap,size,i,pos);
	}

}