# README #

This README would normally document whatever steps are necessary to get your application up and running.

### What is this repository for? ###

* Quick summary

  This program auto generates a dungeon with a size 
of 80 unit horizontally and 21 units vertically counting borders.
There is a minimum of five rooms and they are at least 3x2in size.
Corridors are generated automatically. There is a buffer of 3 units
between rooms. Rooms are denoted as yellow colored '.' and corridors 
are '#'. 
  A default of 5 monsters will be created and maximum amount of
monsters will be the amount of rooms spaces avaliable except 4 units
away from player spawn point. Program uses ncurses to render the dungeon
and you can move the player using the number keys. You can kill monsters
by moving into thier space on your turn, but the monster can kill you by
moving on your space in the monster's turn.Players and Monsters move at 
a turn speed of 1000/speed. Some monsters can tunnel, some are smart and 
will use the terrain to their advantage, some are erratic and move 
randomly sometimes, and there are telepathic monsters that will know the 
player position. For ease of visibility, intelligent monsters are blue
and erratic monsters are red. With Erratic monsters taking priority in
color. The player location also blinks. 
  The control scheme for the player is shown below. 5 or space is the wait 
button. Speed of turns in real time is porportional to the speed of the 
player or monster. The Player cannot tunnel through walls, so attempting 
to go through walls will not work. There is no penalty for trying and 
you're allowed to select another move. 
  When a player is on a '<' or '>', you can press '<' or '>' depending on 
terrain to ascend or descend. If you ascend stairs, you'll be placed on the
downstairs of the next floor. If you descend stairs, you will be placed on
the upstairs of the next floor.
  Pressing 'm' will open a list of monsters with relative coordinates from 
the player. While in list mode, if there are more monsters than the list 
can display in one time then you can scroll down the list with the down 
key. Pressing Esc while in list mode, will close the list. The List will
display a maximum of 14 monsters and will show the monster's ID, Type,
and coordinates relative to player using general cardinal NESW conventions. 


Pressing 'Q' will exit the game. 

Figure 1: Default Controls
7  8  9
 \ | /
4--5--6
 / | \
1  2  3

y  k  u
 \ | /
h-- --l
 / | \
b  j  n

Table 1: Player Controls
7 or y 		Attempt to move PC up and left.
8 or k 		Attempts to move PC up.
9 or u 		Attempts to move PC up and right.
4 or h 		Attempt to move PC left.
6 or l 		Attempts to move PC right.
1 or b 		Attempt to move PC down and left.
2 or j 		Attempts to move PC down.
3 or n 		Attempts to move PC down and right.
>		Player Goes down stairs when on a '>' terrain.
<		Player Goes up stairs when on a '<' terrain.
m 		Opens a Monster List and thier relative positions from Player.
Up_Arrow 	Scrolls Up the Monster List assuming you aren't at the top.
Down_Arrow	Scrolls Down the Monster List assuming you aren't at the bottom.
Escape		Closes the Monster List
Q		Quits the Game. Prints out End Game Message.

*Other Info

  Valgrind had been used to check this program and shows that there is
memory still reachable due to ncurses holding the memory prior to exiting
the game. 

Version 1.0

Repo for update to date source is at :

https://bitbucket.org/Salzarin/kieu_don.assignment-1.05/

### How do I get set up? ###

* Summary of set up

	Run make in directory and it'll build the Rogue Like
	Dungeon:
	
		In Directory, run:
		
		make
		
		then run:
		
		./Dungeon

* How to Run Details

	In executable directory, Run:
	
	./Dungeon
	
	This will generate a random dungeon and you can play the dungeon
until you hit 'q' on your turn, get killed by a monster, or you kill all
the monsters.
	
	You can choose the amount of monsters by using the -nummon flag.
    The maximum about is dependant on how much room is availiable in their
    dungeon.
    
    ./Dungeon -nummon 10
    
    If you want the player to shamble around aimlessly -auto. Press 'q'
    at anytime to exit.
    
    ./Dungeon -auto

	You can randomize the number of rooms with the arguement flag -s. 
For example,

	./Dungeon -s 10
	
	However, the program will only accept anything above 5. Any value 
less than 5 will default to 5 rooms. It will also keep trying to lay a
room 2000 times before it stops and prints out an error message and sets
the max rooms to where it had stopped at. 

	You can also set the player position. However, if your player position,
is invalid, then the program will randomly set a player position.

  For example, this will load a map and set the player position (1,2), but since it
the player isn't in the room, the program will select a valid player position at
(37,16):
	
	./Dungeon --load -p 1 2

	
	
-p 1 2 arg is not in a room.
37, 16
--------------------------------------------------------------------------------
|                                                                              |
|             .....                          #######                           |
|             .....#######  ##################   # #                 ....      |
|             .....      # ##                    # #          ...####....      |
|                  ############  .........    ......###    ###...    ....      |
|                  #          ## .........#   ......  ###  #  ...     ##       |
| ....##############           # .........#      #      ####          #        |
| ....#            #    ....   # ####     ####   #         ##         #        |
| ....#            #    ....#######          #####          ######### #        |
| ....             #    ....#     #         ####                    ###        |
| ....        ....##    ....      ##        #  ##                     #        |
| ....        ....# ####....       ##    ####   ## .........        ........   |
| ....        ....###   ....        #    #       ##.........        ........   |
| ....        ....      ....        #    #         .........        ........   |
|                                   #   ##         .........                   |
|                                   .@.......      .........                   |
|                                   .........      .........                   |
|                                                                              |
|                                                                              |
--------------------------------------------------------------------------------

	
	
	This program implements a binary heap using Dikjstra's Algorithm.
    Ncurses was used to make the interface and movement of player.
    In addition, the queue system is implemented the same way as in class.
	
*Advanced Details
	
	Flags:
	  -s #
	Allows to generate rooms with the number of rooms, #, specified.
	This flag is ignored when loading a valid dungeon file.
	
	-l filename
	Specifies the filename for loading and saving. Can only support up
      to 50 characters long filename.
	
	--save
	Saves the Dungeon at $(HOME)/.rlg327/dungeon as default. 
    With '-l' flag, it will save with what ever the '-l' flag filename specifies.
    
	--load
	Loads the Dungeon at $(HOME)/.rlg327/dungeon as default. 
    With '-l' flag, it will save with what ever the '-l' flag filename specifies.
	
	-p x y
	Set the player position (x,y). The coordinate system is given below.
	
	----->x
	|
	|
	|
	v
	y

	-nummon
	Specifies the maximum amount of monsters. Will default to 5 without flag.
	
	-pspeed
	Will specify the player speed. Defaults to 10 without flag.
	
	-v
	Verbose trigger to print the events every turn. Refresh rate of the console
	will update 1Hz.
	
	-auto
	Disables control and player moves automatically to a random room erratically. 
	Press Q to quit.
	

* Repo owner or admin
Don Kieu
donsterxx@gmail.com
