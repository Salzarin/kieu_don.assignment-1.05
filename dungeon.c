#include "dungeon.h"




#ifdef _WIN32
#define ROOMCOLOR ""
#define RESET ""
#else
#define ROOMCOLOR "\x1b[30;43m"
#define RESET "\x1b[0m"
#endif

int compareLocation(int from, int to){
  if((to-from)>0){
    return 1;
  }
  else if(to==from){
  
    return 0;
  }
  else{
    return -1;
  }
  
}

int printMonsterList(player p, monster_list_t * monsters){
  monster_node_t* itr = monsters->head;
  int size = 0;
  int key = 0;
  int i;
  int start = 0;
  int w = MAX_H*3/4;
  int h = MAX_V*3/4;
   
  char lat[][6] = {"North", "", "South"};
  char lon[][6] = {"West", "", "East"};
  int list_size = h;
  
  WINDOW* monsterList = newwin(h,w,(MAX_V-h)/2,(MAX_H-w)/2);
  
  keypad(monsterList,TRUE);

  size = queue_size(monsters);
  set_escdelay(0);
  while(key!=27 && key!='Q'){
    wclear(monsterList);
    itr = monsters->head;
    for(i = 0; i<start;i++){
      itr = itr->next;
    }
    for(i=start; i<(start+list_size) && itr;i++){
	wprintw(monsterList, "%d Monster %1x ", i, itr->data.stats.attr);
	monster m = itr->data;
	if(compareLocation(p.x,m.x)!=0){
	wprintw(monsterList, "%d %s",abs(m.x-p.x),lon[compareLocation(p.x,m.x)+1]);
	  if(compareLocation(p.y,m.y)!=0){
	    wprintw(monsterList, ", ");
	  }
	}
	if(compareLocation(p.y,m.y)!=0){
	wprintw(monsterList, "%d %s\n",abs(m.y-p.y),lat[compareLocation(p.y,m.y)+1]);
	}
	else{
	wprintw(monsterList,"\n");  
	}
	itr = itr->next;
    }
    
    
    key = wgetch(monsterList);
    printw("%d", key);
    wrefresh(monsterList);
    
    
    if(key == KEY_UP){
      
      if(start>0){
	start--;
      }
    }
    
    if(key == KEY_DOWN){
      if((start+list_size)<size){
	start++;
      }
    }
    
  }
  endwin();
   return key;
  
}

void initAllColors(){
  
  init_pair(1, COLOR_BLACK, COLOR_YELLOW);//Dungeon background
  init_pair(2, COLOR_RED,COLOR_BLACK); //Erratic
  init_pair(3, COLOR_YELLOW,COLOR_BLACK); 
  init_pair(4, COLOR_GREEN,COLOR_BLACK);
  init_pair(5, COLOR_BLUE,COLOR_BLACK); //Intelligent
  init_pair(6, COLOR_CYAN,COLOR_BLACK);
  init_pair(7, COLOR_MAGENTA,COLOR_BLACK);

  init_pair(12, COLOR_RED,COLOR_YELLOW); //Erratic
  init_pair(13, COLOR_YELLOW,COLOR_YELLOW); 
  init_pair(14, COLOR_GREEN,COLOR_YELLOW);
  init_pair(15, COLOR_BLUE,COLOR_YELLOW); //Intelligent
  init_pair(16, COLOR_CYAN,COLOR_YELLOW);
  init_pair(17, COLOR_MAGENTA,COLOR_YELLOW);
  init_pair(8, COLOR_WHITE,COLOR_RED);
  
  
  
}
bool validKeyCheck(int key){
  char check[] = "123456789Qykulnjbh ";
  int size = strlen(check);
  int i; 
  for(i = 0; i<size;i++){
    if(check[i]==(unsigned char)(key)){
      return true;
    }
  }
  return false;
}


int translateKeyinput(int key){
  int val = 9;
  switch(key){
    case 49:
      val = 0;
      break;
    case 50:
      val = 1;
      break;
    case 51:
      val = 2;
      break;
    case 52:
      val = 7;
      break;
    case 54:
      val = 3;
      break;
    case 55:
      val = 6;
      break;
    case 56:
      val = 5;
      break;
    case 57:
      val = 4;
      break;
    case 'b':
      val = 0;
      break;
    case 'j':
      val = 1;
      break;
    case 'n':
      val = 2;
      break;
    case 'h':
      val = 7;
      break;
    case 'l':
      val = 3;
      break;
    case 'y':
      val = 6;
      break;
    case 'k':
      val = 5;
      break;
    case 'u':
      val = 4;
      break;
      
    default:
      break;
  }

  return val;
}

int getnewDirection(int d, int *x,int *y){
  int _DX[8]={-1,0,1,1, 1, 0,-1,-1};  
  int _DY[8]={ 1,1,1,0,-1,-1,-1, 0};
  
  int new = d;
  if(d<8){
  *x = _DX[new];
  *y = _DY[new];
  }

  
  return new;
}

int getnewRandDirection(int d, int *x,int *y){
  int _DX[8]={-1,0,1,1, 1, 0,-1,-1};  
  int _DY[8]={ 1,1,1,0,-1,-1,-1, 0};
  
  int new = d;//(d+(rand()%5 -2))%8;
  //new = new<0?new+8:new;
  *x = _DX[new];
  *y = _DY[new];

  
  return new;
}



data endianSwap(data e){
  data final;
  final.str[0] = e.str[3];
  final.str[1] = e.str[2];
  final.str[2] = e.str[1];
  final.str[3] = e.str[0];
  return final;
}

bool checkEndian(){
  int i = 1;
  
  return (*(char *)&i == 1);
  
}

bool checkRoomIntersection(room A, room B){

int Aw = A.w;
int Ah = A.h;
int Bw = B.w;
int Bh = B.h;
int buffer = 2;

//Add one to the bounds, so the bounding box
//guarantees that we have 1 space between rooms
int Aleft = A.x -buffer;
int Atop = A.y-buffer;
int Aright = Aleft+Aw+buffer; 
int Abot = A.y+Ah+buffer;

int Bleft = B.x-buffer;
int Btop = B.y-buffer;
int Bright = Bleft+Bw+buffer; 
int Bbot = B.y+Bh+buffer;

	//Bound Check, if any one of these are true,
	// no overlap can exist.
	if(Aright < Bleft) return false;
	if(Aleft > Bright) return false;
	if(Atop > Bbot) return false;
	if(Abot < Btop) return false;
	
				
	//We're returning True if it overlaps
	return true;
}


int createDungeon(dungeon_t* d){

	//Reset All Parameters of the Dungeon
	memset(d->dungeonmap,0,MAX_V*MAX_H*sizeof(int));
	memset(d->rockhardnessmap,0,MAX_V*MAX_H*sizeof(int));
	memset(d->PNTMap,0,sizeof(d->PNTMap));
	memset(d->PTMap,0,sizeof(d->PTMap));
	memset(d->PDestMap,0,sizeof(d->PDestMap));
	
	
	initializeDungeon(d,true);
  


	int i, j;
	//Initialize the Hardness Map. For now make it random hardness.
	for(j = 0;j<MAX_V;j++){
		for(i=0;i<MAX_H;i++){
		    
		    d->rockhardnessmap[i][j] = (rand() % 253) +1; 
		}
		
	}
	
	//Make Bounds
	for(i = 0; i<MAX_V; i++){
		d->rockhardnessmap[0][i] = 255; 
		d->rockhardnessmap[MAX_H-1][i] = 255; 
	}

	for(i = 0; i<MAX_H; i++){
		d->rockhardnessmap[i][0] = 255; 
		d->rockhardnessmap[i][MAX_V-1] = 255; 
	}
	


	//Create Room Positions
	//Bound Width is equal to the Max Bounds
	//subtracted by the left and right wall 
	//and a buffer of 2 offsets.
	//The reasoning is for room drawable space.
	int BoundWidth = MAX_H -4;
	int BoundHeight = MAX_V - 4;

	//Set Max Room Width and Height
	int MaxRoomWidth = 7;
	int MaxRoomHeight = 8;
	int MinRoomWidth = 3;
	int MinRoomHeight = 2;

	int offset = 2;
	//Start Random Generation of Rooms
	for(i = 0; i<d->Max_Rooms;i++){

		int x = rand()%BoundWidth + offset;
		int y = rand()%BoundHeight + offset;
		d->Room[i].x = x;
		d->Room[i].y = y;
		d->Room[i].w = rand()%MaxRoomWidth + MinRoomWidth; 
		d->Room[i].h = rand()%MaxRoomHeight + MinRoomHeight;
		d->Room[i].cx = x + d->Room[i].w/2;
		d->Room[i].cy = y + d->Room[i].h/2;
		 
		//Initially say the room isn't valid, to run through loop.
		d->Room[i].valid = false;

		//Debug Check Top Left, Center, Bottom Right;

		int attempt = 0;
		while(((d->Room[i].x + d->Room[i].w)>(MAX_H-2)) || 
			((d->Room[i].y + d->Room[i].h)>(MAX_V-2))||
			!d->Room[i].valid){

			
			int intersectioncount = 0;
			
			x = rand()%BoundWidth + offset;
			y = rand()%BoundHeight + offset;
			d->Room[i].x = x;
			d->Room[i].y = y;
			d->Room[i].w = rand()%MaxRoomWidth + MinRoomWidth; 
			d->Room[i].h = rand()%MaxRoomHeight + MinRoomHeight;
			d->Room[i].cx = x + d->Room[i].w/2;
			d->Room[i].cy = y + d->Room[i].h/2;	
			
			int k = 0;
			
			for(k = 0; k<i ; k++){
		
				if(checkRoomIntersection(d->Room[k],d->Room[i]) && k!=i){
					d->Room[i].valid = false;
					intersectioncount++;

					//Debug Check for Intersection and with what.
					//printf("attempt %d : Room Intersection with Room %d and %d and with %d rooms.\n",attempt,k,i,intersectioncount);


					attempt++;
				}
			}
			if(intersectioncount == 0){			
				d->Room[i].valid = true;
			}

			if(attempt > 4999){
				printf("Not enough space for new room, Max Rooms is now : %d.\n", i-1);
				d->Max_Rooms = i-1;
				break;		
			}
			
		}
		
	}
	
	

	drawCorridors(d);
	
	drawCorridorsHardness(d);
	
	drawRooms(d);

	placeStairs(d, 0);
	placeStairs(d, 1);
	
	while(d->downStairs.x == d->upStairs.x && d->downStairs.y == d->upStairs.y){
	  placeStairs(d, 1);
	}
	drawStairs(d);
	
	//printDungeon(dungeonmap);

	  


	return d->Max_Rooms;
}

void placeStairs(dungeon_t *d, int type){
  
  stairs_t stairs;
  int cRoom = rand() % d->Max_Rooms;
  int x = d->Room[cRoom].x;
  int y = d->Room[cRoom].y;
  int w = d->Room[cRoom].w;
  int h = d->Room[cRoom].h;
  
  stairs.x = x + rand() % w;
  stairs.y = y + rand() % h;
  if(type){
  d->upStairs = stairs;  
  }
  else{
  d->downStairs = stairs;  
  }
  
  
}


void drawStairs(dungeon_t *d){
  
  d->dungeonmap[d->downStairs.x][d->downStairs.y] = '<';
  d->dungeonmap[d->upStairs.x][d->upStairs.y] = '>';
}


void drawCorridorsHardness(dungeon_t* d){
  int i,j;
  for(j = 0; j<MAX_V;j++){
    for(i = 0;i<MAX_H;i++){
    
	if(d->dungeonmap[i][j] == '#'){
	  d->rockhardnessmap[i][j] = 0;
	}
      
    }
    
  }
  
  
}


void initializeDungeon(dungeon_t *d, bool hardness_set){
	int i;
  	//Make Bounds
	for(i = 0; i<MAX_V; i++){
	  
	  if(d->rockhardnessmap[0][i] == 255){
	    d->dungeonmap[0][i] = '|';
	  }
	  
	  if(d->rockhardnessmap[MAX_H-1][i] ==255){
	    d->dungeonmap[MAX_H-1][i] = '|';
	  }
	  
	  if(hardness_set){
	    d->rockhardnessmap[0][i] = 255;
	    d->rockhardnessmap[MAX_V-1][i] = 255;
	    d->dungeonmap[0][i] = '|';
	    d->dungeonmap[MAX_H-1][i] = '|';
	  }
	  
	}

	for(i = 0; i<MAX_H; i++){
	  if(d->rockhardnessmap[i][0] == 255){
	    d->dungeonmap[i][0] = '-'; 
	  }
	  if(d->rockhardnessmap[i][MAX_V-1]==255){
	    d->dungeonmap[i][MAX_V-1] = '-';
	  }
	  
	  if(hardness_set){
	    d->rockhardnessmap[i][0] = 255;
	    d->rockhardnessmap[i][MAX_V-1] = 255;
	    d->dungeonmap[i][0] = '-';
	    d->dungeonmap[i][MAX_V-1] = '-';
	  }
	}
}



void printDungeon(dungeon_t * d){
	int i,j;
  	//Print Dungeon Map
	
	for(j = 0; j<MAX_V;j++){ 
		for(i = 0; i<MAX_H;i++){
			if(d->dungeonmap[i][j] == 0){
				putchar(' ');
			}
			else{
				if(d->dungeonmap[i][j] == '.')
				printf("\x1b[30;43m");
				putchar(d->dungeonmap[i][j]);
				printf("\x1b[0m");
			}
		}
		putchar('\n');
	}
	putchar('\n');
}

void inCursesPrintDungeon(dungeon_t d){
	int i, j;
	
  	for(j = 0; j<MAX_V;j++){ 
		for(i = 0; i<MAX_H;i++){
			if(d.dungeonmap[i][j] == 0){
				mvaddch(j+1, i, ' ');
			}
			else{
				if(d.dungeonmap[i][j] == '.')
				attron(COLOR_PAIR(1));
				mvaddch(j+1,i, d.dungeonmap[i][j]);
				attroff(COLOR_PAIR(1));
			}
		}
		printw("\n");
	}
	printw("\n");
  
}

void drawRooms(dungeon_t * d){
	int i;
  	//Draw Rooms
	for( i = 0; i<d->Max_Rooms;i++){
		int m;
		int n;
		for(m = d->Room[i].x; m<(d->Room[i].x+d->Room[i].w);m++){
			for(n = d->Room[i].y; n<(d->Room[i].y+d->Room[i].h);n++){
				d->dungeonmap[m][n] = '.';
				d->rockhardnessmap[m][n] = 0;
			}
		}
	}
  
}

void loadCorridors(dungeon_t * d){
  int i,j;
  for(j = 0; j<MAX_V; j++){
    for(i = 0; i<MAX_H;i++){
      if(d->rockhardnessmap[i][j] == 0){
      
	d->dungeonmap[i][j] = '#';
      }      
    }
  }
  
}

void drawCorridors(dungeon_t * d){
	
	int i;
	//Draw Paths To Rooms
	for(i = 0;i<(d->Max_Rooms-1);i++){
		//Copy the Current Hardness Map.
		int tempHardnessMap[MAX_H][MAX_V];
		memcpy(tempHardnessMap,d->rockhardnessmap,sizeof(tempHardnessMap));
		
		
		
		int m, n, k;
		
		
		//For All Other Rooms, set hardness map as if it was a wall.
		for( k = 0; k<5;k++){
			if(k!=i && k!=(i+1)){
				for(m = (d->Room[k].x-1); m<(d->Room[k].x+d->Room[k].w+1);m++){
					for(n = (d->Room[k].y-1); n<(d->Room[k].y+d->Room[k].h+1);n++){
						if(m>0 && m<MAX_H && n>0 && n<MAX_V){
							tempHardnessMap[m][n] = 255;
						}
					}
				}
			}
		}
		//Debug Print New Hardness Map
		/*
		for(m = 0;m<MAX_V;m++){
			for(n=0;n<MAX_H;n++){
				if(tempHardnessMap[n][m] < 10){
				putchar(48+tempHardnessMap[n][m]);
				
				}
				else{
					putchar(' ');
				}	
			}
		putchar('\n');
		}
		putchar('\n');
		*/
		//Convert to 1D array for dijkstra's
		int i_start = d->Room[i].cx + d->Room[i].cy*MAX_H; 
		int i_end = d->Room[i+1].cx + d->Room[i+1].cy*MAX_H;
		//Calculate Shortest Path via Dijkstra's Algorithm.
		
		pList path;
		int length;
		int lengtharray[MAX_V*MAX_H];
		int parray[MAX_V*MAX_H];
		path = dikstras(tempHardnessMap, i_start, i_end, false, false, &length,lengtharray, parray);
		
		for(k = 0;k<path.size;k++){
			int x = path.List[k] % MAX_H;
			int y = path.List[k] / MAX_H;
			d->dungeonmap[x][y] = '#';
		}
		
	}
	
}



int loadDungeon(char * filename,dungeon_t* d){
  char* file = malloc(strlen(getenv("HOME"))+strlen("/.rlg327/")+50);
  strcpy(file,getenv("HOME"));
  strcat(file,"/.rlg327/");
  strcat(file,filename);
  FILE* loadfile;
  loadfile = fopen(file, "r");
  
  if(loadfile == NULL){
    printf("File Error : %s\n", strerror(errno));
    return -1;
    
  }
  
  int result = 0;
  int max_file_size;
  
  result++;
  
  
  fseek(loadfile,0,SEEK_END);
  max_file_size = ftell(loadfile);
  rewind(loadfile);
  
  //printf("File Size is %d\n",max_file_size);
  
  
  char * file_data = malloc(max_file_size);
  result = fread(file_data,sizeof(char),max_file_size,loadfile);
  
  //printf("Read %d\n",result);
  char marker[6];
  data version;
  data size;
  
  int pos = 0;
  memcpy(marker,file_data,sizeof(marker)+pos);
  //printf("Header: %s\n",marker);
  pos+=sizeof(marker);
  
  memcpy(version.str,file_data+pos,sizeof(version.str));
  pos+=sizeof(version.str);
  if(checkEndian()){
    version = endianSwap(version);
  }
  //printf("Version: %d\n", version.integer);
  
  memcpy(size.str,file_data+pos,sizeof(size.str));
  pos+=sizeof(size.str);
  if(checkEndian()){
    size = endianSwap(size);
  }
  //printf("file_size: %d\n", size.integer);  
  
  unsigned char map[MAX_V][MAX_H];
  memcpy(map,file_data+pos,sizeof(map));
  
  int i, j;  
  for(j = 0;j<MAX_V;j++){
    for(i = 0; i<MAX_H;i++){
      d->rockhardnessmap[i][j] = (int)map[j][i];
    }
  }
  pos+=(MAX_V*MAX_H);
  
  d->Max_Rooms = (max_file_size - pos) / 4;
  //printf("There are %d rooms.\n", Max_Rooms);
  
  
  for(i = 0; i<d->Max_Rooms; i++){
    d->Room[i].y = (int)file_data[pos+i*4];
    d->Room[i].x = (int)file_data[pos+i*4+1];
    d->Room[i].h = (int)file_data[pos+i*4+2];
    d->Room[i].w = (int)file_data[pos+i*4+3];
    d->Room[i].cx = d->Room[i].x + d->Room[i].w/2;
    d->Room[i].cy = d->Room[i].y + d->Room[i].h/2;
      
    //Initially say the room isn't valid, to run through loop.
    d->Room[i].valid = false;
    
    //printf("%d-> Y:%d X:%d H:%d W:%d\n", i, Rooms[i].y, 
	//   Rooms[i].x, Rooms[i].h, Rooms[i].w);
  }
  
  /*
  for(i = 0; i<(Max_Rooms*4); ++i){
  
    printf("%d ",file_data[pos+i]);
  }
  */
  
  
	//Initialize Dungeon Map
	//int dungeonmap[MAX_H][MAX_V];
	//memset(dungeonmap,0,sizeof(dungeonmap));
	
	initializeDungeon(d,false);
	
	loadCorridors(d);
	
	drawRooms(d);


	//printDungeon(dungeonmap);
	fclose(loadfile);
	free(file);
	free(file_data);
  
return d->Max_Rooms;
}

int saveDungeon(char * filename,dungeon_t* d){

  
  unsigned char hardnessmap[MAX_V][MAX_H];
  int i,j;

  for(j=0;j<MAX_V;j++){
    for(i=0;i<MAX_H;i++){
      hardnessmap[j][i] = (unsigned char)(d->rockhardnessmap[i][j]);
    }
  }
  
  
  char* path = malloc(strlen(getenv("HOME"))+strlen("/.rlg327/")+1);
  char* file = malloc(strlen(getenv("HOME"))+strlen("/.rlg327/")+50);
  strcpy(path,getenv("HOME"));
  strcat(path,"/.rlg327/");

  //printf("%s\n",path);
  
  mkdir(path,0777);
  char mark[7] = "RLG327";
  
  data version;
  version.integer= 0000;
  
  unsigned char hard_map[MAX_H*MAX_V];
  
  memcpy(hard_map,hardnessmap,sizeof(hardnessmap));
  data size;
  size.integer = (int)(sizeof(int)+sizeof(version)+6+sizeof(hardnessmap) + d->Max_Rooms*4);
  //printf("Total Data to be Written: %d\n", size.integer);
  size = endianSwap(size);
  version = endianSwap(version);
  
  strcpy(file,getenv("HOME"));
  strcat(file,"/.rlg327/");
  strcat(file,filename);
  printf("%s\n",size.str);
  FILE* savefile;
  savefile = fopen(file, "w");
  int result;
  if(savefile == NULL){
    printf("File Error : %s", strerror(errno));
    return d->Max_Rooms;
    
  }
  
  result = fwrite(mark,sizeof(char),6, savefile);
  //printf("%d, %s\n", result, mark);
  result = fwrite(version.str,sizeof(char),sizeof(version.str), savefile);
  //printf("%d, %d\n", result, version.integer);
  result = fwrite(size.str,sizeof(char),sizeof(size.str),savefile);
  //printf("%d, %d\n", result, size.integer);
  result = fwrite(hardnessmap, sizeof(unsigned char), sizeof(hardnessmap), savefile);
  //printf("%d\n", result);
  result++;

  
  for(i=0;i<d->Max_Rooms;i++){
    
    data roomdata;
    roomdata.str[0]=(unsigned char)(d->Room[i].y);
    roomdata.str[1]=(unsigned char)(d->Room[i].x);
    roomdata.str[2]=(unsigned char)(d->Room[i].h);
    roomdata.str[3]=(unsigned char)(d->Room[i].w);
    
    
    fwrite(roomdata.str, sizeof(char), sizeof(roomdata.str), savefile);
    //printf("%d-> Y:%d X:%d H:%d W:%d\n", i, (int)roomdata.str[0], 
    //(int)roomdata.str[1], (int)roomdata.str[2], (int)roomdata.str[3]);
    
  }
  
  
  
  fclose(savefile);
  
  free(path);
  free(file);
  
return 0;
}




void renderDungeon(player p, dungeon_t d,monster_list_t* mList){
  
    clear();
    inCursesPrintDungeon(d);
    printPlayer(p);
    printMonsters(mList);  
}

void gameoverMessage(player p, dungeon_t d,monster_list_t* mList){
  
    move(21+1,0);// Move to End of the line
  
    refresh();
    renderDungeon(p,d,mList);
  
    
    move(21+1,0);
    int key = 0;
    timeout(-1);
  while(key!='Q'){
    move(21+1,0);
    if(p._isAlive){
      if(queue_size(mList) > 0){
	printw("You Win! Unless you rage quit. Which I'm sure you did, %d enemies left.\n", queue_size(mList));
      }
      else{
	printw("You Win! PETA is going to have your head.\n");
      }
    }
    else{
      printw("You Died, Noob.\n");
    }
    
    printw("Press Q to quit\n");
    key = getch();
    renderDungeon(p,d,mList);
    
  }
  
  
  
}


void init_Events(player p,dungeon_t d,character events[5000], int* eventSize, int eventPos[5000], monster_list_t * mList, monster_node_t ** monsters, int* nummon, struct timespec ti){

  

  
  //Initalize Event Queue
  

  
  queue_init(mList);  
  initMonster(p,d, mList, monsters,nummon);

  
  printf("Generating %d Monsters.\n",*nummon);  
  createNoTunnelMap(p, d.rockhardnessmap,d.NoneTunnelMap,d.PNTMap);
  createTunnelMap(p, d.rockhardnessmap,d.TunnelMap,d.PTMap);
  

  
  events[0].id = 0;
  events[0].eventTime = 0;//Player starts first
  events[0].player = true;
  events[0]._isAlive = true;
  p._isAlive = true;
  events[0].timeCreated = time(&ti.tv_sec);
  events[0].timeCreatedNano = ti.tv_nsec;
  cPush(events,events[0], eventSize,eventPos);
  
  monster_node_t * itr = mList->head;
  int i;
  for(i = 1;itr; i++){
    events[i].id = i;
    events[i].player = false;
    events[i]._isAlive = true;
    events[i].eventTime = 1000/monsters[i-1]->data.speed;
    monsters[i-1]->data._isAlive = true;
    events[i].mNode = monsters[i-1];
    events[i].timeCreated = time(&ti.tv_sec);
    events[i].timeCreatedNano = ti.tv_nsec;
    cPush(events,events[i], eventSize,eventPos);
    itr = itr->next;
  }
  
  
}

void cleanEvents(character events[5000], int* eventSize, int eventPos[5000], monster_list_t * mList, monster_node_t ** monsters){

    //Check if the Event Queue Already Exists
  if(*eventSize || queue_size(mList)){
    while(*eventSize){
      cPop(events,eventSize,eventPos);
    }
    
    queue_delete(mList);
    free(monsters);
    free(mList);
  }
  
}




