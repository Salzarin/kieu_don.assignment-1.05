#include "dungeon.h"

void createTunnelMap(player p, int hardmap[MAX_H][MAX_V],int Tmap[MAX_H][MAX_V], int PTMap[MAX_V*MAX_H]){
    int end = p.x+p.y*MAX_H;
    int i,j;
    int lengthArray[MAX_H*MAX_V];
    int distance;
    
    dikstras(hardmap, end, end, true, true, &distance, lengthArray,PTMap);
    
    for(j = 0; j<MAX_V;j++){
      for(i = 0;i<MAX_H;i++){
	distance = INT_MAX; 
	int index = i+j*MAX_H;
	
	if(index != end){
	  if(hardmap[i][j]!=255){
	    //dikstras(hardmap, index, end, true, true, &distance, lengthArray);
	    distance = lengthArray[index];
	    //printf("%d", distance%10);
	  }
	  if(hardmap[i][j] != 255){
	  Tmap[i][j] = distance;
	  }
	  else{
	  Tmap[i][j] = distance;
	  }
	    
	}
	
	else{
	  //printf(" ");
	  Tmap[i][j] = 0;
	}
	
      }
      //printf("\n");
    }
   //printf("\n");
}


void createNoTunnelMap(player p, int hardmap[MAX_H][MAX_V], int Tmap[MAX_H][MAX_V], int PNTmap[MAX_H*MAX_V]){

    int end = p.x+p.y*MAX_H;
    int i,j;
    int lengthArray[MAX_V*MAX_H];
    int distance;
    
    
    dikstras(hardmap, end, end, true, false, &distance, lengthArray,PNTmap);
    for(j = 0; j<MAX_V;j++){
      for(i = 0;i<MAX_H;i++){
	distance = INT_MAX;
	int index = i+j*MAX_H;
	if(index != end){
	  if(hardmap[i][j] == 0){
	    
	    Tmap[i][j] = lengthArray[index];
	  }
	  else{
	  Tmap[i][j] = distance;
	  }
	}
	else{
	  //printf(" ");
	  Tmap[i][j] = 0;
	}
	
      }
      //printf("\n");
    }
   //printf("\n");
  
}


void initMonster(player p, dungeon_t d, monster_list_t * e, monster_node_t ** monsters, int *nummon){
  
  
  
  int i;
  
  bool setmon[MAX_H][MAX_V];
  memset(setmon,0,sizeof(setmon));
  setmon[p.x][p.y] = true;
  
  for(i=0;i<*nummon;i++){
    monster m;
    int cRoom = rand() % d.Max_Rooms;
    int x = d.Room[cRoom].x;
    int y = d.Room[cRoom].y;
    int w = d.Room[cRoom].w;
    int h = d.Room[cRoom].h;
    
    m.x = x + rand() % w;
    m.y = y + rand() % h;
    
    int dest_x = m.x - p.x;
    int dest_y = m.y - p.y;
    
    int dist = dest_x*dest_x+dest_y*dest_y;
    int count = 0;
    while((dist<16 || setmon[m.x][m.y])){
      
      cRoom = rand() % d.Max_Rooms;
      x = d.Room[cRoom].x;
      y = d.Room[cRoom].y;
      w = d.Room[cRoom].w;
      h = d.Room[cRoom].h;
	
	
      m.x = x + rand() % w;
      m.y = y + rand() % h;
      dest_x = m.x - p.x;
      dest_y = m.y - p.y;
      dist=dest_x*dest_x+dest_y*dest_y;

      if(count > 999){
	printf("Can't Render %d Monsters, not enough room.\n",*nummon);
	(*nummon) = i-1;
	
	return;
      }
      count++;
    }
    
    
    int j;
    int attr = 0;
    for(j=0;j<4;j++){
      int randAttr = rand()%2;
      if(!randAttr){
	attr += 1<<j;
      }
    }
    
    
    m.stats.attr =attr;
    m.speed = rand()%16+5;
    m.speed = m.speed>20 ? 20: m.speed;
    m._isAlive = true;
    m._seen = false;
    m.dest_x = m.x;
    m.dest_y = m.y;
    m.inRoom = true;
    m.id = i;
    setmon[m.x][m.y] = true;
    queue_add(e,&monsters[i],m);
  }

  
}

void printMonsters(monster_list_t *monsters){

    monster_node_t * monster_node = monsters->head;
    while(monster_node){  
    bool intelligent = monster_node->data.stats.attr    & 0x1;
    //bool telepath = monster_node->data.stats.attr >> 1  & 0x1;
    //bool tunneler = monster_node->data.stats.attr >> 2  & 0x1;
    bool erratic = monster_node->data.stats.attr >>  3  & 0x1;
    int color_code = 0;
    
    if(intelligent){
      color_code = 5;
    }
    else if (erratic){
      color_code = 2;
    }
    
    if(monster_node->data.inRoom && color_code){
	color_code+=10;
    }
    
    if(monster_node->data._isAlive){
      if(color_code != 0){
	attron(COLOR_PAIR(color_code));
	mvprintw(monster_node->data.y+1,monster_node->data.x,"%1x", monster_node->data.stats.attr);
	attroff(COLOR_PAIR(color_code));
      }
      else{
	mvprintw(monster_node->data.y+1,monster_node->data.x,"%1x", monster_node->data.stats.attr);
      }     
    }
    monster_node=monster_node->next;
    }
}


void monsterMovement(player p, monster *mon, int PNTmap[MAX_H*MAX_V], int PTmap[MAX_H*MAX_V],int Hmap[MAX_H][MAX_V], int *new_x, int *new_y){
  bool intelligent, telepath, tunneler, erratic;
  
  intelligent = mon->stats.attr & 0x01;
  telepath = (mon->stats.attr>>1) & 0x01;
  tunneler = (mon->stats.attr>>2) & 0x01;
  erratic = (mon->stats.attr>>3) & 0x01; 
  
  int cur_x = mon->x;
  int cur_y = mon->y;
  int index = cur_x+cur_y*MAX_H;
  int new_index; 

  //If monster is erratic then see if it moves erratically  
  if(erratic){  
    if(rand()%2){
      //Erratic Movement
      int dx = 0;
      int dy = 0;
      int check_x =-1;
      int check_y = -1;
      while((check_x<1 || check_x>(MAX_H-2) || check_y< 1 || check_y >(MAX_V-2))){
	dx = 0;
	dy = 0;
	getnewDirection(rand()%8, &dx,&dy);
	check_x = mon->x +dx;
	check_y = mon->y +dy;
      }
      *new_x = mon->x +dx;
      *new_y = mon->y +dy;
      return;
    }
  }
  //First Check if you know where you're going
  //Knows Player Location or Last know position
  if(telepath){
  //Intelligent uses map  
    if(intelligent){
      //Selects Which Map it uses
      
      if(tunneler){
	new_index = PTmap[index];
      }
      else{
	new_index = PNTmap[index];
      }
      //Move Monster Toward Player
      if(new_index>0){
	*new_x = new_index % MAX_H;
	*new_y = new_index / MAX_H;
      }
    }
    else{
      //Go Straight Toward Player
      int dx = p.x - mon->x;
      int dy = p.y - mon->y;
      dx = dx < 0 ? -1: dx == 0 ? 0 : 1;
      dy = dy < 0 ? -1: dy == 0 ? 0 : 1;
      *new_x = mon->x +dx;
      *new_y = mon->y +dy;
      
    }
  }
  else{
    int dx = mon->dest_x - mon->x;
    int dy = mon->dest_y - mon->y;
    dx = dx < 0 ? -1: (dx == 0 ? 0 : 1);
    dy = dy < 0 ? -1: (dy == 0 ? 0 : 1);
    *new_x = mon->x + dx;
    *new_y = mon->y + dy;
  }

  
  
} 
  
bool checkLos(monster *mon, player p, dungeon_t d){
  int x = mon->x;
  int y = mon->y;
  while(!(x==p.x && y==p.y)){
    int dx = p.x - x;
    int dy = p.y - y;
    dx = dx < 0 ? -1: dx == 0 ? 0 : 1;
    dy = dy < 0 ? -1: dy == 0 ? 0 : 1;
    x +=dx;
    y +=dy;
    if(d.rockhardnessmap[x][y]>0){
      mon->_seen = false;
      return false;
    }
  }

    mon->dest_x = p.x;
    mon->dest_y = p.y;
    mon->_seen = true;

  
  return true;
}



