#ifndef MONSTER_H
#define MONSTER_H

#include <stdlib.h>
#include <unistd.h>
#include <stdbool.h>
#include <ncurses.h>

typedef struct monsterdata{
  int x;
  int y;
  int id;
  union stats{
    bool _attr[8];
    unsigned char attr;   
  }stats;
  int dest_x;
  int dest_y;
  int speed;
  bool _isAlive;
  bool _seen;
  int last_heading;
  bool inRoom;
} monster;


typedef struct monster_node_t{
  monster data;
  struct monster_node_t *next, *prev;  
}monster_node_t;

typedef struct monster_list{
  int size;
  monster_node_t *head, *tail;
} monster_list_t;

int queue_init(monster_list_t *e);
int queue_delete(monster_list_t *e);
int queue_add(monster_list_t *e,monster_node_t ** mn, monster m);
int queue_remove(monster_list_t *e, monster *m);
int queue_size(monster_list_t *e);
int queue_remove_at(monster_list_t *e, monster_node_t *m);



#endif