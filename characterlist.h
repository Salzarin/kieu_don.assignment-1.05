#ifndef CHARACTERLIST_H
#define CHARACTERLIST_H
#include "monster.h"
#include <limits.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>
#include <stdbool.h>
#include <string.h>

//Used to Build Edges and Node Tree



typedef struct character_t
{
  int eventTime;
  int id;
  bool _isAlive;
  monster_node_t* mNode;
  bool player;
  unsigned int timeCreated;
  unsigned int timeCreatedNano;
  
} character;

//Add Function Prototypes
void cHeap(character Heap[], int size, int check, int pos[]);
void cDecreaseKey(character Heap[], character Node, int pos[]);
character cPop(character Heap[], int *size, int pos[]);
void cPush(character Heap[],character c, int *size, int pos[]);
void cBuildHeap(character Heap[], int size, int pos[]);
int cLeft(int i);
int cRight(int i);
int cParent(int i);
#endif
