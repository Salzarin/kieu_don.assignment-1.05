#include "dijkstras.h"


pList dikstras(int map[MAX_H][MAX_V], int start, int end, bool monster, bool tunneler, int *length, int pathArray[1680], int PMap[MAX_V*MAX_H]){


int i,j;
int total_vertex = MAX_H*MAX_V;
edgeQueue_t * aList[total_vertex];

//Set our Node List to Null
for(i=0;i<total_vertex;i++){
	aList[i] = malloc(sizeof(edgeQueue_t));
	edgeQueue_init(aList[i]);
}

//Traverse Through Hardness Map And Add Edges
//Basically building our network of nodes.

for(j=0;j<MAX_V;j++){
	for(i = 0; i<=MAX_H;i++){
		int index = i+j*MAX_H;
		int cells[8];
		cells[0] = index-MAX_H;//Up
		cells[1] = index+1;//Right
		cells[2] = index+MAX_H;//Down
		cells[3] = index-1;//Left
		
		cells[4] = index-MAX_H-1;//Up-Left
		cells[5] = index-MAX_H+1;//Up-Right
		cells[6] = index+MAX_H+1;//Down-Right
		cells[7] = index+MAX_H-1;//Down-Left		
		
		int Max_Cells = 4;
		if(monster){
		  Max_Cells = 8;
		}
		
		int n;
		for(n = 0;n<Max_Cells;n++){
			if(cells[n]>= 0 && cells[n]<(MAX_H*MAX_V)){
				int x = cells[n]%MAX_H;
				int y = cells[n]/MAX_H;
				if(x>0 && x<MAX_H && y>0 && y<MAX_V){
				  
				  if(map[x][y] != 255){ //As Long as it's not a wall
				    
				    int hard = map[x][y];
				    
				    if(monster){ //If it is a monster
				      
				      
				      
				      if(tunneler){
					if(hard+1 != 255){
					hard = hard==0 ? 1 : 1+(hard)/85;
					edgeQueue_add(aList[index],cells[n],hard);
					}
					//aList[index] = addEdge(aList[index],cells[n],hard);
				      }
				      else{
					if(hard == 0){
					  edgeQueue_add(aList[index],cells[n],1);
					  //aList[index] = addEdge(aList[index],cells[n],1);
					}					
				      }
				    }
				    else{ //If it is cooridor
				      edgeQueue_add(aList[index],cells[n],hard);
				      //aList[index] = addEdge(aList[index],cells[n],hard);
				    }
				  }
				  
				}
			}			
		}
		
	}
}


vertex_t min;

int distances[total_vertex];
int parent[total_vertex];
vertex_t pQ[total_vertex];
int pos[total_vertex];


//Initial all routes.
for(i = 0; i<total_vertex;i++){
	distances[i] = INT_MAX;
	parent[i] = -1;
	pQ[i].dist = INT_MAX;
	pQ[i].vertex = i;
	pos[i] = i;
}

//Set Distance of Source to zero

distances[start] = 0;
pQ[start].dist = 0;
buildHeap(pQ,total_vertex,pos);

for(i = 0; i<total_vertex;i++){
	min = pop(pQ,total_vertex,pos);
	
	//node_t * trav = aList[min.vertex];
	int size = edgeQueue_size(aList[min.vertex]);
	while(size){
		int u = min.vertex;
		//int v = trav->vertex;
		//int w = trav->weight;
		int v,w;
		edgeQueue_remove(aList[min.vertex],&v,&w);
		if(distances[u] != INT_MAX && (distances[v] > (distances[u]+w))){
			//printf("%d, %d , %d \n", i, v, total_vertex);
			distances[v] = distances[u]+w;
			parent[v] = u;
			vertex_t changed;
			changed.vertex = v;
			changed.dist = distances[v];
			decreaseKey(pQ,changed,pos);
			
		}
		size = edgeQueue_size(aList[min.vertex]);
		//trav=trav->next;
		
	}
	
}



i = parent[end];
j = 0;

*length = distances[end];

memcpy(pathArray,distances,sizeof(distances));

pList p;

if(i>0 && distances[i]!=INT_MAX){
  while(start!=parent[i] && parent[i] != -1){
	i=parent[i];
	p.List[j] = i;
	j++;
	p.size = j;
  }
}
for(i = 0; i<total_vertex;i++){  
  
  pathArray[i]-=(1+map[i%80][i/80]/85)-(1+map[start%80][start/80]/85);
  PMap[i] = parent[i];
  
edgeQueue_delete(aList[i]);
free(aList[i]);  
}

return p;
}
